<?php

namespace App\BLL;

use App\Entity\Contacto;
use Doctrine\ORM\EntityManagerInterface;

class ContactoBLL
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function guarda(Contacto $contacto)
    {
        $this->em->persist($contacto);
        $this->em->flush();
    }
}