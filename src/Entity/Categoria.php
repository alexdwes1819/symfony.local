<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $numContactos;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Contacto",
     *     mappedBy="categoria"
     * )
     * @var Collection $contactos
     */
    private $contactos;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getNumContactos(): ?int
    {
        return $this->numContactos;
    }

    public function setNumContactos(int $numContactos): self
    {
        $this->numContactos = $numContactos;

        return $this;
    }

    /**
     * @return Collection Contacto
     */
    public function getContactos(): Collection
    {
        return $this->contactos;
    }
}
