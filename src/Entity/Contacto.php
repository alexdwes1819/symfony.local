<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactoRepository")
 */
class Contacto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El campo nombre no puede quedar vacío")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El campo nombre no puede quedar vacío")
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El campo nombre no puede quedar vacío")
     * @Assert\Length(
     *     min=9,
     *     max=9,
     *     exactMessage="La longitud del teléfono debe ser de 9 caracteres"
     * )
     */
    private $telefono;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="App\Entity\Categoria",
     *     inversedBy="contactos"
     * )
     * @ORM\JoinColumn()
     */
    private $categoria;

    /**
     * @Assert\GreaterThan(
     *     value = 18
     * )
     */
    private $edad;

    /**
     * @ORM\Column(type="string")
     */
    private $imagen;

    /**
     * @Assert\File(mimeTypes={ "image/png", "image/jpeg" })
     * @Assert\NotBlank(message="Tienes que subir la imagen del contacto")
     */
    private $fileImagen;

    /**
     * @Assert\IsTrue(message="El nombre y la dirección no pueden ser iguales")
     */
    public function isNombreDireccionDistintos()
    {
        return !($this->getNombre() === $this->getDireccion());
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     * @return Contacto
     */
    public function setDireccion($direccion): self
    {
        $this->direccion = $direccion;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEdad()
    {
        return $this->edad;
    }

    /**
     * @param mixed $edad
     * @return Contacto
     */
    public function setEdad($edad)
    {
        $this->edad = $edad;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     * @return Contacto
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     * @return Contacto
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFileImagen()
    {
        return $this->fileImagen;
    }

    /**
     * @param mixed $fileImagen
     * @return Contacto
     */
    public function setFileImagen($fileImagen)
    {
        $this->fileImagen = $fileImagen;
        return $this;
    }


}
