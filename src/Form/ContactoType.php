<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 24/01/19
 * Time: 17:46
 */

namespace App\Form;

use App\Entity\Categoria;
use App\Entity\Contacto;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactoType extends AbstractType
{
    public function buildForm(
        FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'nombre',
                TextType::class)
            ->add(
                'telefono',
                TextType::class)
            ->add(
                'direccion',
                TextType::class,
                [
                    'label' => 'Dirección'
                ])
            ->add(
                'categoria',
                EntityType::class,
                [
                    'class' => Categoria::class,
                    'choice_label' => 'nombre',
                    'label' => 'Categoría'
                ])
            ->add(
                'edad',
                NumberType::class,
                [
                    'label' => 'Edad'
                ])
            ->add(
                'fileImagen',
                FileType::class,
                [
                    'label' => 'Imagen (PNG/JPG)'
                ])
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'Crear contacto'
                ])
        ;
    }
    public function configureOptions(
        OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contacto::class,
        ]);
    }
}