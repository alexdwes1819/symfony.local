<?php

namespace App\EventListener;

use App\Entity\Contacto;
use App\Helper\FileUploader;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploadListener
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    private function uploadFile($entity)
    {
        if (!$entity instanceof Contacto)
            return;

        $file = $entity->getFileImagen();

        if ($file instanceof UploadedFile)
        {
            $fileName = $this->uploader->upload($file);
            $entity->setImagen($fileName);
            $entity->setFileImagen($file);
        }
    }

    public function prePersist(
        LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->uploadFile($entity);
    }

    public function preUpdate(
        PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->uploadFile($entity);
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Contacto)
            return;

        if ($fileName = $entity->getImagen()) {
            $entity->setImagen($fileName);
            $entity->setFileImagen(
                new UploadedFile(
                    $this->uploader->getTargetDir() . '/' . $fileName,
                    $fileName)
            );
        }
    }
}