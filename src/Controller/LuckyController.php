<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lucky")
 */
class LuckyController extends AbstractController
{
    /**
     * @Route("/", name="app_lucky_inicio")
     */
    public function inicio()
    {
        return $this->render('inicio.html.twig');
    }

    /**
     * @Route(
     *     "/numbers/{inicio}/{fin}",
     *     name="app_lucky_number",
     *     requirements={
     *          "inicio"="\d+",
     *          "fin"="\d+"
     *     })
     */
    public function number($inicio=1, $fin=100)
    {
        $number = mt_rand($inicio, $fin);

        return $this->render(
            'number.html.twig',
            [
                'number' => $number
            ]);
    }
}