<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 18/01/19
 * Time: 19:55
 */

namespace App\Controller\admin;

use App\BLL\ContactoBLL;
use App\Entity\Contacto;
use App\Form\ContactoType;
use App\Repository\ContactoRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contactos")
 */
class ContactoController extends AbstractController
{
    private function getContactoForm(Contacto $contacto=null)
    {
        if(is_null($contacto))
        {
            $url = $this->generateUrl('app_contactos_new');
            $contacto = new Contacto();
        }
        else
        {
            $url = $this->generateUrl(
                'app_contactos_save_update',
                [
                    'id' => $contacto->getId()
                ]);
        }

        if (is_null($url))
            $url = $this->generateUrl('app_contactos_new');

        return $this->createForm(
            ContactoType::class,
            $contacto,
            [
                'action' => $url
            ]);
    }

    private function showContactos($form)
    {
        $contactosRepository = $this->getDoctrine()->getRepository(
            Contacto::class);

        $contactos = $contactosRepository->findAll();

        $categoriaPrimerContacto = $contactos[0]->getCategoria()->getNombre();

        $fechaActual = new \DateTime();

        return [
            'contactos' => $contactos,
            'form' => $form->createView(),
            'fechaActual' => $fechaActual,
            'categoriaPrimerContacto' => $categoriaPrimerContacto
        ];

    }
    /**
     * @Route("", name="app_contactos", methods={"get"})
     * @Template("contactos.html.twig")
     */
    public function listar(Request $request)
    {
        $form = $this->getContactoForm();

        return $this->showContactos($form);
    }

    private function guardaContacto(
        Request $request,
        ContactoBLL $contactoBLL,
        Contacto $contacto=null)
    {
        $form = $this->getContactoForm($contacto);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $contacto = $form->getData();

            $contactoBLL->guarda($contacto);

            return $this->redirectToRoute('app_contactos');
        }

        $this->getDoctrine()->getManager()->clear();

        return $this->showContactos($form);
    }
    /**
     * @Route(
     *     "",
     *     name="app_contactos_new",
     *     methods={"post"})
     * @Template("contactos.html.twig")
     */
    public function nuevo(
        Request $request,
        ContactoBLL $contactoBLL)
    {
        return $this->guardaContacto(
            $request, $contactoBLL);
    }

    /**
     * @Route(
     *     "/{id}/delete",
     *     name="app_contactos_delete",
     *     requirements={
     *          "id"="\d+"
     *     })
     */
    public function delete(Contacto $contacto=null)
    {
        if (is_null($contacto))
            throw new NotFoundHttpException('No se ha encontrado el contacto buscado');

        $em = $this->getDoctrine()->getManager();

        $em->remove($contacto);
        $em->flush();

        return $this->redirectToRoute('app_contactos');
    }

    /**
     * @Route(
     *     "/{id}/save-update",
     *     name="app_contactos_save_update",
     *     methods={"post"})
     * @Template("contactos.html.twig")
     */
    public function guardaModificado(
        Request $request,
        ContactoBLL $contactoBLL,
        Contacto $contacto)
    {
        return $this->guardaContacto(
            $request, $contactoBLL, $contacto);
    }

    /**
     * @Route(
     *     "/{id}/update",
     *     name="app_contactos_update",
     *     requirements={
     *          "id"="\d+"
     *     })
     * @Template("contactos.html.twig")
     */
    public function update(Contacto $contacto=null)
    {
        $form = $this->getContactoForm($contacto);

        return $this->showContactos($form);
    }

    /**
     * @Route(
     *     "/buscar",
     *     name="app_contactos_buscar",
     *     requirements={
     *          "id"="\d+"
     *     },
     *     methods={"post"})
     * @Template("contactos.html.twig")
     */
    public function buscar(
        Request $request,
        ContactoRepository $contactoRepository)
    {
        $form = $this->getContactoForm();

        $busqueda = $request->request->get('busqueda');
        $edad = $request->request->get('edad');

        $contactos = $contactoRepository->findContactosPorPrefijo($busqueda);

        $fechaActual = new \DateTime();

        return [
            'contactos' => $contactos,
            'form' => $form->createView(),
            'fechaActual' => $fechaActual
        ];
    }
}